$(document).ready(function(){


    var nameVM = function(name, surname) {
        var self = this;

        self.name = name;
        self.surname = surname;

    };

    var playgroundVM = function() {

        var self = this;

        self.name = ko.observable('');
        self.surname = ko.observable('');

        self.names = ko.observableArray();

        self.test = ko.computed(function(){

                //self.names.push(new nameVM(self.name(), self.surname()));

                return self.name() + " " + self.surname();
            }
        );

    };

    ko.applyBindings( new playgroundVM() );


});